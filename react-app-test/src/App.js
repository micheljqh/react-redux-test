import React, { Component } from 'react';
import './App.css';
import Counter from './components/Counter';

class App extends Component {
  render() {
    const { store } = this.props;
    return (
      <div className="App">
        OHH, this is a test: Michel
        <Counter
          value={store.getState()}
          onIncrement={(value) => store.dispatch({ type: 'INCREMENT', data: value })}
          onDecrement={() => store.dispatch({ type: 'DECREMENT' })}
        />
      </div>
    );
  }
}

export default App;
