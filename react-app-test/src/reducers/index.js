import React from 'react';
export default (state = [], action) => {
  switch (action.type) {
    case 'INCREMENT':
      state.push(<div style={{ textAlign: 'left', padding: 5, margin: 5, border: 'solid 1px red' }}>{action.data}</div>)
      break;
    case 'DECREMENT':
      state.pop()
      break;
    default:
      return state
  }
  return state;
}