import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: 'test'
        };
    }

    handleChange(e) {
        this.setState({ data: e.target.value });
    }

    render() {
        const { value, onIncrement, onDecrement } = this.props;
        return (
            <div>
                <input type="text" value={this.state.data} onChange={this.handleChange.bind(this)} />
                <p>
                    <button onClick={onIncrement.bind(this, this.state.data)}>Add text</button>
                    {' '}
                    <button onClick={onDecrement}>Remove last</button>
                </p>
                <div>{value}</div>
            </div>
        )
    }
}

Counter.propTypes = {
    value: PropTypes.array.isRequired,
    onIncrement: PropTypes.func.isRequired,
    onDecrement: PropTypes.func.isRequired
}

export default Counter